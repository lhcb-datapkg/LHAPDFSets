export LHAPDFSETSDATA=$(shell pwd)/data

all: .downloadPDFs.stamp

.downloadPDFs.stamp: .FORCE
	md5sum data/lhapdf.conf cmt/download_pdfs.py > $@.tmp
	test -e $@ || touch $@
	diff -q $@ $@.tmp || (./cmt/download_pdfs.py && mv -f $@.tmp $@)

clean: 
	rm -f .downloadPDFs.stamp
	rm data/pdfsets.index  data/pdfs.md5
	find data/ -mindepth 1 -type d -print0 | xargs -0 /bin/rm -rf

.FORCE:

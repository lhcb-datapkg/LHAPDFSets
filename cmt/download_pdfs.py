#!/usr/bin/env python

# downloadd_pdfs.py:
#
# Download the PDF sets to the data directory of the package.
#
# This script downloads the PDF sets from the official LHAPDF
# repository. The full dictionary of PDF names and members used by
# LHCb is in 'LHAPDFSetsDict'; to change the list available, this file
# should be updated and the package released.

import sys
import os
import logging

# Version of LHAPDF sets to access.
LHAPDFSetsVers = '6.2.3'

# Dictionary of PDF sets and required members.
LHAPDFSetsDict = {'cteq6l1': [0],
                  'CT09MCS': [0],
                  'CT10nlo': [0],
                  'MSTW2008lo68cl': [0],
                  'MSTW2008nlo68cl': [0],
                  'cteq66': [0],
                  'MRSTMCal': [0],
                  'MRST2007lomod': [0],
                  'MMHT2014lo68cl': [0],
                  'NNPDF23_lo_as_0130_qed':range(0,101)}

LHAPDF_SRC_URL = ('http://lhapdfsets.web.cern.ch/lhapdfsets/current/')

class LHAPDFSetsTool:

    def __init__(self):
        """ 
        Initilize the class.
        """
        FORMAT = '%(asctime)-15s %(levelname)s %(message)s'
        logging.basicConfig(format=FORMAT)
        self.log = logging.getLogger('LHAPDFSetTool')
        self.log.setLevel(logging.INFO)
        self.mTargetDir = None

    def getTargetDir(self):
        """
        Retrieve the file location based on the environment (and cache it).
        """
        if self.mTargetDir != None:
            return self.mTargetDir
        
        # Get the target directory from the environment.
        if not 'LHAPDFSETSDATA' in  os.environ.keys():
            raise Exception('LHAPDFSETSDATA environment variable not set!')

        # Return.
        self.mTargetDir = os.environ['LHAPDFSETSDATA']
        self.log.info('Target directory: %s' % self.mTargetDir)
        return self.mTargetDir

    def downloadPDFs(self):
        """
        Download to the PDFs to the data directory.
        """
        # Download the index.
        import urllib
        if sys.version_info[0] > 2:
            import urllib.request

        targetdir = self.getTargetDir()
        url = LHAPDF_SRC_URL + 'pdfsets.index'
        idx = os.path.join(targetdir, 'pdfsets.index')
        self.log.info('Downloading %s to %s' % (url, idx))
        if sys.version_info[0] > 2:
            urllib.request.urlretrieve(url, idx)
        else:
            urllib.urlretrieve(url, idx)

        # Iterate over the PDFs.
        for pdf, mems in iter(LHAPDFSetsDict.items()):
            tgz = os.path.join(targetdir, pdf + '.tar.gz')
            url = LHAPDF_SRC_URL + pdf + '.tar.gz'

            # Retrieve the tgz.
            self.log.info('Downloading %s to %s' % (url, tgz))
            if sys.version_info[0] > 2:
                urllib.request.urlretrieve(url, tgz)
            else:
                urllib.urlretrieve(url, tgz)

            # Extract the PDF members.
            try:
                self.log.info('Extracting %s' % tgz)
                import tarfile
                pdfdir = tarfile.open(tgz, 'r:gz')
                pdfdir.extractall(targetdir)
                os.remove(tgz)
            except:
                raise Exception('Failed to extract %s' % tgz)
                
            # Remove non-relevant members.
            names = os.listdir(os.path.join(targetdir, pdf))
            for name in names:
                if name == pdf + '.info': continue
                mem = name.replace(pdf + '_', '')
                mem = mem.replace('.dat', '')
                try: mem = int(mem)
                except: pass
                if mem in mems: continue
                self.log.info('Removing %s' % name)
                os.remove(os.path.join(targetdir, os.path.join(pdf, name)))

    def checkFile(self, name):
        """
        Check a single file and return checksum.
        """
        targetdir = self.getTargetDir()
        fullname  = os.path.join(targetdir, name)
        if not os.path.exists(fullname): 
            raise Exception('Missing PDF file %s' % name)
            return (name, 'missing')
        else:
            import hashlib
            return (name, hashlib.md5(open(fullname, 'rb').read()).hexdigest())
    
    def checkFiles(self):
        """
        Check that all files are there and compute checksum.
        """
        targetdir = self.getTargetDir()
        checksums = []
        for pdf, mems in sorted(iter(LHAPDFSetsDict.items())):
            name = '%s/%s.%s' % (pdf, pdf, 'info')
            checksums.append(self.checkFile(name))
            mems = sorted(mems)
            for mem in mems:
                name = '%s/%s_%04d.dat' % (pdf, pdf, mem)
                checksums.append(self.checkFile(name))

        # Save the checksums.
        checksumfile = open(os.path.join(targetdir, 'pdfs.md5'), 'w')
        for (name, checksum) in checksums:
            checksumfile.write('%s\t%s\n' % (name, checksum))
        checksumfile.close()

    def run(self):
        try:
            self.downloadPDFs()
            self.checkFiles()
        except Exception as e:
            self.log.error('Error downloading PDFs.')
            self.log.exception(e)
            return 1

if __name__ == '__main__':
    tool = LHAPDFSetsTool()
    sys.exit(tool.run())
